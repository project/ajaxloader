
ajaxLoader Module
-----------------------------------------------
This is a helper module that allows someone to easily 
add AJAX style content replacement to their theme. This requires 
some knowledge of theming and drupal.

Essentially, this module allows for a particular kind of link 
formating that allows for non-javascript browsers to degrade 
nicely and for javascript browsers to easily swap content.

It also adds support for loading content which itself contains
AJAX style content replacement. See implementation notes if
you wish to implement this

Currently this module supports: 
 * views
 * node teasers
 * single taxonomy terms
 * taxonomy pager
 * form elements

Thanks to!
-----------------------------------------------
jonathan @ civicactions . com for helping me understand why 
replaced content needs help with javascript


How To Implement
-----------------------------------------------
You need to do at least two things in your theme: 
 1) make a link 
 2) write a function to handle your link
 3) OPTIONAL if you're including new content into your page 
    which also has ajax functionality, you need to register
    your function with the postloader

// Link format
<a class="MYCLASS" href="NO/JAVASCRIPT/PATH?ajaxLoaderCall=ajaxloader/view&view=my_view_name&replaceDiv=ajaxLoaderContent">Load my view</a>

MYCLASS - class used to pick up link clicks via jquery
NO/JAVASCRIPT/PATH - path to page if user doesn't have javascript enabled
ajaxLoaderCall=ajaxloader/view - what path to execute to get ajax content replacement
view=my_view_name - name of view 
replaceDiv=ajaxLoaderContent - name of div to put new content into

Some other kinds of loads:
 * ajaxloader/node/teaser/NID 
 * ajaxloader/term/TID (will also parse the &page=X )
 * ajaxloader/form/MY_DRUPAL_FORM - returns a partiallly (no <form> tag) rendered form

// Here's some PHP code examples

// TAXONOMY
drupal_add_js('
// gets a single taxonomy term
function ajaxloader_term(){
  $("a.ajaxLoaderTerm").click(
    function (){
      $(".SiteNavListItem.focus_term").removeClass("On");
      $(this).parent("li").addClass("On");      
      ajaxLoaderExecute(this, "ajaxloader_term_pager");
      $("fflAjaxContent").show;
      return false; 
    }
  );
}
$(document).ready(function () {ajaxloader_term();});
', 'inline');

// TAXONOMY PAGER
drupal_add_js('
// replaces pager links for ajax
function ajaxloader_term_pager(){
  $("a.ajaxLoaderTermPager").click(
    function (){ 
      ajaxLoaderExecute(this);
      return false; 
    }
  );
}

ajaxLoaderPostLoad("ajaxloader_term_pager");
$(document).ready(function () {ajaxloader_term_pager();});
','inline');


// NODE TEASER
drupal_add_js('
function ajaxLoaderNodeTeaser(
   $(".ajaxLoaderNode").click(
     function (){  
       ajaxLoaderExecute(this);  
       return false;
     }
   );
}
$(document).ready(function () {ajaxLoaderNodeTeaser();});
', 'inline');

// VIEW
drupal_add_js('
function ajaxLoaderGetView(){
  $(".ajaxLoaderView").click(
    function (){
      // you can always add things like:
      // $(this).addClass("On"); 
      ajaxLoaderExecute(this);
      return false;
    }
  );
}
$(document).ready(function () {ajaxLoaderGetView();});
', 'inline');

// FORM
// This example is quite a bit more complicated. Suppose we have a taxonomy
// that is hierarchical like so:
// - plants
// -- carrots
// -- strawberries
// - animals
// -- dogs
// -- cats
// 
// and we want to have a drop down with the top level terms and then
// dynamically load the sub terms into a new select box
// to do this, we create two forms and some jquery

/**
 * creates the form a browse navigation element
 */
function my_browse_form(){
  // get  vocab
  $taxonomy = taxonomy_get_tree(1, 0, -1, 1);
  $terms[] = t('[any term]');
  foreach($taxonomy as $term){
    $terms[$term->tid] = $term->name; 
  }
  $form['issue'] = array(
    '#title' => t('Kinds'),
    '#type' => 'select',
    '#options' => $terms,
    '#suffix' => '<div id="ajaxLoaderMyForm"></div>',
    '#attributes' => array('id' => 'MyFormSelect'),
  );
  return $form;
}

/**
 * this creates a form for the ajaxloader module to insert 
 * based on the above forms is selected
 */
function my_browse_sub_display_form(){
  // get the term
  $term = $_GET['term'];
  
  // load the terms under this term
  $taxonomy = taxonomy_get_tree(1, $term);
  $terms[] = t('[any term]');
  foreach($taxonomy as $term){
    $terms[$term->tid] = $term->name; 
  }
  
  $form['sub'] = array(
    '#title' => t('Refine'),
    '#type' => 'select',
    '#options' => $terms,
  );
  return $form;
}


drupal_add_js('
function ajaxLoaderGetForm(){
  $("#MyFormSelect").change( 
    function () { 
      // get the term
      var term = document.getElementById(MyFormSelect").value;
      // add content to the div with the requested term
      ajaxLoaderReplace(ajaxLoaderPath()+"ajaxloader/form/my_browse_sub_display_form?term="+term, "ajaxLoaderMyFormDiv");
    }     
  );    
}

$(document).ready(function () {ajaxLoaderGetForm();});
','inline');


// HOW TO CREATE LINKS

// load a view
$output ='<a class="ajaxLoaderView" href="'. base_path() . 'path/to/view?ajaxLoaderCall=ajaxloader/view&view=my_view_name&replaceDiv=ajaxLoaderContent">Load my view</a>

// load a taxononmy term page
$output ='<a class="ajaxLoaderTerm" href="'. base_path() .'taxonomy/term/'. $term->tid . '?ajaxLoaderCall=ajaxloader/term/' . $term->tid . '&replaceDiv=ajaxLoaderContent">'.$term->name .'</a>

// load a node teaser
$output ='<a class="ajaxLoaderNode" href="'. base_path() . 'node/'. $node->nid .'?ajaxLoaderCall=ajaxloader/node/teaser/'. $node->nid .'&replaceDiv=ajaxLoaderContent">Load my node teaser</a>

$output ='<div id="ajaxLoaderContent"></div>';