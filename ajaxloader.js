/* *********************************** */
/* AJAX LOADER JS FILE */
/* *********************************** */

/**
 * does the actual fetching of content and replacement
 * @url is a url to fetch, fully qualified
 * @replace_div is the div id to place the new content
 */
function ajaxLoaderReplace(url, replace_div){
  // fetch data from url
  $.get(url, '',
    function(data){
      // @TODO this is hard coded for now, probably should 
      //       have some way of allowing individual functions
      //       to overfide this
      $("#"+replace_div).fadeOut("slow", 
        function(){
          $("#"+replace_div).empty();
          $("#"+replace_div).hide();
        
          // add the data in
          $("#"+replace_div).append(data);
        
		      // run any functions which have have requested
		      // to be run once after replacement
		      ajaxLoaderPostLoad("", true);
         
      		$("#"+replace_div).fadeIn("slow");
        }
      );
    }
  );
}


/**
 * helper function to parse a url string for variables 
 * @variable is a variable name to look for
 * @url is the string to search
 * @return the variable value if found
 */
function ajaxLoaderGetQueryVariable(variable,url){
  var vars = url.split("?");
  if(vars[1]){ vars = vars[1].split("&"); }
  for (var i=0;i<vars.length;i++) { 
    var pair = vars[i].split("="); 
    if (pair[0] == variable) { 
      return pair[1]; 
    } 
  } 
}

/**
 * inital call to prepar for the content fetching
 * @obj is the a object that has requested content
 */
function ajaxLoaderExecute(obj){
  var href = $(obj).attr("href");
  var args = href.split("?");
  var path = ajaxLoaderPath()+ajaxLoaderGetQueryVariable("ajaxLoaderCall", href)+"?"+args[1];
  // it can be the case that is_rpc is already appened to the URL
  if(! ajaxLoaderGetQueryVariable("is_rpc", href)) {
    path == "&is_rpc=1"; 
  }
  var replaceDiv = ajaxLoaderGetQueryVariable("replaceDiv", href);
  ajaxLoaderReplace(path, replaceDiv);  
  return false;
}

// set up the global value for post loading  
var ajaxLoaderPostLoadFunctions = new Array();

/**
 * internal function to register functions which need to 
 * be reloaded after content is placed on the page, for example
 * when content has been loaded that has .click() functions 
 * in the content
 * @function_name name of the function to rerun 
 * @run runs all the functions that have been registered 
 */
function ajaxLoaderPostLoad(function_name, run){
  if(function_name){
    ajaxLoaderPostLoadFunctions.push(function_name);
    return;
  }
  if(run){
    for ( var i=0, len=ajaxLoaderPostLoadFunctions.length; i<len; ++i ){
      //potential security issue
      if (eval ('typeof '+ajaxLoaderPostLoadFunctions[i]+'=="function"')){
        eval(ajaxLoaderPostLoadFunctions[i]+'()');
      }
    }
  }
}


/**
 * replaces pager links for ajax
 */
function ajaxloader_term_pager(){
  $("a.ajaxLoaderTermPager").click(
    function (){ 
      ajaxLoaderExecute(this);
      return false; 
    }
  );
}

/**
 * replaces pager links for ajax
 */
function ajaxloader_view_pager(){
  $("a.ajaxLoaderViewPager").click(
    function (){ 
      ajaxLoaderExecute(this);
      return false; 
    }
  );
}

// load these functions for the post page loader
ajaxLoaderPostLoad("ajaxloader_term_pager");
ajaxLoaderPostLoad("ajaxloader_view_pager");

// load these functions on page load
$(document).ready(function () {
  ajaxloader_term_pager();
  ajaxloader_view_pager
  }
);
